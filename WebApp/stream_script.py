import streamlit as st
import base64
import pandas as pd
import time
from torchvision import models, transforms
import torch.nn as nn
import torch
import os
from skimage import io
from pathlib import Path
import dropbox
import shutil

def model_predict(model_name):
    model_eval = models.resnet101(pretrained=True)
    class_output = {
        'binary': {'Ghost': 0, 'Animal': 1, 'Unknown': 2},
        'class': {'Ghost': 0, 'Aves': 1, 'Mammalia': 2, 'Unknown': 3},
        'species': {
            'Ghost': 0,
            'ArremonAurantiirostris': 1,
            'Aves (Class)': 2,
            'BosTaurus': 3,
            'CaluromysPhilander': 4,
            'CerdocyonThous': 5,
            'CuniculusPaca': 6,
            'Dasyprocta (Genus)': 7,
            'DasypusNovemcinctus': 8,
            'DidelphisAurita': 9,
            'EiraBarbara': 10,
            'Equus (Genus)': 11,
            'Leopardus (Genus)': 12,
            'LeptotilaPallida': 13,
            'Mammalia (Class)': 14,
            'MazamaAmericana': 15,
            'Metachirus (Genus)': 16,
            'Momota (Genus)': 17,
            'Nasua (Genus)': 18,
            'PecariTajacu': 19,
            'ProcyonCancrivorus': 20,
            'Rodentia (Order)': 21,
            'Sciurus (Genus)': 22,
            'SusScrofa': 23,
            'TamanduaTetradactyla': 24,
            'TinamusMajor': 25,
            'Unknown': 26}
    }

    model_map = class_output[model_name]
    in_features = len(model_map)
    reverse_model_map = {v: k for k, v in model_map.items()}

    using_gpu = torch.cuda.is_available()
    if using_gpu:
        st.write('### Image Prediction Starts. Using GPU!')
    else:
        st.write('### Image Prediction Starts. Using CPU!')
    time.sleep(2)
    device = torch.device("cuda:0" if using_gpu else "cpu")

    num_ftrs = model_eval.fc.in_features
    model_eval.fc = nn.Linear(num_ftrs, in_features)

    model_eval = model_eval.to(device)

    model_full_name = sorted([i for i in os.listdir(
        './models/') if model_name.lower() in i.split('_')], reverse=True)[0]

    try:
        model_eval.load_state_dict(torch.load(
            f'./models/{model_full_name}',  map_location=device))
        st.write(f'### Loading {model_full_name} pre-trained model')
    except Exception as e:
        st.write(f'### cannot load model! {e}')
        raise(e)
    time.sleep(1)

    regulated_size = 300, 450
    default_val_transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize(size=regulated_size),
        transforms.ToTensor(),
    ])
    model_eval.eval()
    softmax = torch.nn.Softmax(dim=1)

    # Dropbox API
    access_token = "IdSksADBFacAAAAAAAAAAcTAT5d77_SGkwjhmQcxC2BHo8ak0B1j_lGKiZqfH3cy"
    dbx = dropbox.Dropbox(access_token)

    img_dropbox = '/MIDS/Model_V1/Input_Data/'
    img_dropbox_to = '/MIDS/Model_V1/Output_Data/'

    img_local = Path('cache/')
    img_local.mkdir(exist_ok=True)
    folder = dbx.files_list_folder(img_dropbox)

    result_list = []
    for file in folder.entries:
        if file.name.lower().endswith('jpg'):
            img_local_path = img_local / file.name
            dbx.files_download_to_file(
                download_path=img_local_path, path=file.path_display)
            tmp_img = io.imread(img_local_path)
            tmp_img = default_val_transform(tmp_img)

            with torch.no_grad():
                inputs = tmp_img.to(device)
                inputs = inputs.reshape(1, *inputs.shape)
                outputs = model_eval(inputs)

                prob = softmax(outputs)
                pred_prob, pred_id = torch.max(prob, 1)
                pred_id = pred_id.tolist()

                pred_prob = pred_prob.tolist()[0]
                pred_str = reverse_model_map[pred_id[0]]
            st.write(
                f'### Image Name: {file.name}; Predicted Class: {pred_str}; Predicted Probability:{pred_prob * 100:.2f}%')
            result_list.append((file.name, pred_str, f'{pred_prob * 100:.2f}%'))
            dbx.files_copy_v2(from_path=file.path_display,
                              to_path=f'{img_dropbox_to}{model_name}/{pred_str}/{file.name}', autorename=True)
    if result_list:
        time.sleep(1)
        result_df =  pd.DataFrame(result_list, columns=['file_name', 'prediction', 'prediction_probability']).set_index('file_name')
        
        shutil.rmtree(img_local)
        st.success('Image Prediction Finished!')
        return result_df
    else:
        st.error('### No Input')
        return None

st.set_page_config(
    page_title="Saving nature animal classification",
    layout="wide",
    initial_sidebar_state="expanded",
)
st.write('## Saving nature animal classification')
with st.sidebar:
    model_name = st.radio('Please select model', ['binary', 'class', 'species'])

    click_start = st.button('Run model')


if click_start:
    with st.empty():
        df = model_predict(model_name)
    st.dataframe(df)

    if df is not None and (not df.empty):
        object_to_download =df.to_csv(index=True)
        b64 = base64.b64encode(object_to_download.encode()).decode()
        download_filename = f'{pd.Timestamp.now().strftime("%Y%m%d_%H%M%S")}_{model_name}.csv'
        download_link_text = 'click here to download the data into csv'
        tmp_download_link = f'<a href="data:file/txt;base64,{b64}" download="{download_filename}">{download_link_text}</a>'
        st.markdown(tmp_download_link, unsafe_allow_html=True)
